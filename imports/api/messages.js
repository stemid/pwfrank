import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Messages = new Mongo.Collection('messages');

// TODO: And check if the route is /message/<Id> and perhaps even check that Id
// is valid, to avoid publishing everything to clients that aren't requesting a
// specific Id already.
if (Meteor.isServer) {
    Meteor.publish('message.current', function messagesPublication(messageId) {
        return Messages.find({_id: messageId});
    });
}

Meteor.methods({
    'messages.insert'(message, maxViews=1, maxDays=1) {
        check(message, String);

        var id = Messages.insert({
            text: message,
            maxViews: maxViews,
            maxDays: maxDays,
            views: 0,
            createdAt: new Date()
        });

        return id;
    },

    // This is for getting a message to show in template because it prepares
    // values only required when viewing messages.
    'messages.get'(messageId) {
        check(messageId, String);

        Messages.update(
            messageId,
            { $inc: { views: 1 }}
        );

        return Messages.findOne(
            {_id: messageId}
        );
    },

    'messages.remove'(messageId) {
        check(messageId, String);

        Messages.remove(messageId);
    }
})