import { Mongo } from 'meteor/mongo';

export const Words = new Mongo.Collection('words');
Words._ensureIndex('word', {unique: 1});

Meteor.methods({
    'get.languages'() {
        var rawWords = Words.rawCollection();
        var syncWordsDistinct = Meteor.wrapAsync(rawWords.distinct, rawWords);
        return syncWordsDistinct('language');
    },

    'generate.password'(language=undefined, size=2) {
        var pipeline = Array();

        if (language) {
            pipeline.push({
                $match: { $or: 
                    [
                        { language: language }
                    ]
                }
            });
        }

        pipeline.push({$sample: {size: size}});

        return Words.aggregate(pipeline);
    }
})