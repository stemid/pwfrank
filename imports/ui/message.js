import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Messages } from '../api/messages.js';
import { check } from 'meteor/check';

import './message.html';

// This takes two Date objects as argument.
function daysDiff(date1, date2) {
    check(date1, Date);
    check(date2, Date);

    var msPerDay = 1000 * 3600 * 24;

    var utc1 = Date.UTC(date1.getFullYear(), date1.getMonth(), date1.getDate());
    var utc2 = Date.UTC(date2.getFullYear(), date2.getMonth(), date2.getDate());

    return Math.floor((utc2 - utc1) / msPerDay);
}

Router.route('/:messageId', function() {
    this.render('GetMessage');
});

Template.GetMessage.onCreated(function () {
    var self = this; // self is easier to use in later callbacks and blocks
    // because 'this' might refer to the current context.

    // Template specific subsribe
    var messageId = Router.current().params.messageId;
    self.subscribe('message.current', messageId);

    // This ReactiveDict is used to show data to the client in the template
    // helpers.
    self.messageDict = new ReactiveDict();

    Meteor.call(
        'messages.get',
        messageId,
        function(error, message) {
            if (error) {
                console.log(error);
            } else {
                if (message === undefined) {
                    return false;
                }
                var viewsLeft = Math.ceil(
                    message.maxViews - message.views
                );

                if (viewsLeft < 0) {
                    viewsLeft = 0;
                }

                var curDate = new Date();
                var daysDelta = daysDiff(curDate, message.createdAt);
                var daysLeft = message.maxDays - daysDelta;

                if (daysDelta < 0) {
                    daysLeft = undefined;
                }

                self.messageDict.set('text', message.text);
                self.messageDict.set('viewsLeft', viewsLeft);
                self.messageDict.set('daysLeft', daysLeft);
            }
        }
    );
});

Template.GetMessage.helpers({
    message: function() {
        var messageDict = Template.instance().messageDict;
        return messageDict.get('text');
    },

    viewsLeft: function() {
        var messageDict = Template.instance().messageDict;
        return messageDict.get('viewsLeft');
    },

    daysLeft: function() {
        var messageDict = Template.instance().messageDict;
        return messageDict.get('daysLeft');
    }
});