import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

import './body.html';


Router.route('/', function() {
    this.render('Index', {
        data: function() {
            return {
                maxDays: this.params.query.maxDays ? this.params.query.maxDays : 1,
                maxViews: this.params.query.maxViews ? this.params.query.maxViews : 1
            }
        }
    });
});

Template.Index.onCreated(function() {
    var self = this;

    self.languagesDict = new ReactiveDict();
    Meteor.call(
        'get.languages',
        function(error, languages) {
            if (error) {
                console.log(error);
            } else {
                self.languagesDict.set('languages', languages);
            }
        }
    );
});

Template.Index.helpers({
    passwordGenerator: function() {
        return Meteor.settings.public.password_generator;
    },

    wordlistLanguages: function() {
        var languagesDict = Template.instance().languagesDict;
        return languagesDict.get('languages');
    }
});

Template.Index.events({
    'click #toggle-more-settings' (event) {
        $('#more-settings').collapse('toggle');
        $('#toggle-more-settings').toggleClass('btn-info');

        // Quick solution to implement default settings to be used as
        // bookmark-links.
        var defaultMaxDays = Router.current().params.query.maxDays;
        var defaultMaxViews = Router.current().params.query.maxViews;

        if (defaultMaxDays) {
            let rangeDiv = $('#retention-days').parent();
            let rangeValue = rangeDiv.find('.range-slider__value');
            let rangeSlider = rangeDiv.find('.range-slider__range');
            if (rangeValue.text() == '1') {
                rangeValue.html(defaultMaxDays);
                rangeSlider.val(defaultMaxDays);
            }
        }

        if (defaultMaxViews) {
            let rangeDiv = $('#retention-views').parent();
            let rangeValue = rangeDiv.find('.range-slider__value');
            let rangeSlider = rangeDiv.find('.range-slider__range');
            if (rangeValue.text() == '1') {
                rangeValue.html(defaultMaxViews);
                rangeSlider.val(defaultMaxViews);
            }
        }

    },

    'submit .message-form' (event) {
        event.preventDefault();

        const target = event.target;
        var button = $('#submit-message-form');

        if (target.message.value.length <= 0) {
            return false;
        }

        button.attr("disabled", true);
        button.button('loading');

        var id = Meteor.call(
            'messages.insert',
            target.message.value,
            target.retentionviews.value,
            target.retentiondays.value,
            function(error, result) {
                // Async call, so use callback to provide return value.
                var linkUrl = Meteor.settings.public.base_url + '/' + result;
                $('#messageLink').val(linkUrl);
                $('#linkModal').modal('show');
                $('#messageLink').on('click', function() {
                    $(this).select();
                });
            }
        );

        target.message.value = '';
        button.button('reset');
        button.attr("disabled", false);
    },

    'input .range-slider' (event) {
        var range = event.target;
        var parentDiv = $(range.parentNode);
        var rangeValue = parentDiv.find('.range-slider__value');
        rangeValue.html(range.value);
    },

    'click #generateRandomPassword' (event) {
        event.preventDefault();

        if (!Meteor.settings.public.password_generator) {
            return false;
        }

        const target = event.target;
        var button = $(target);
        var form = target.form;
        var messageInput = $('#messageInput');
        var languageradio = form.languageradio;
        var language = undefined;
        var passwordsize = form.passwordsize;

        button.attr("disabled", true);
        button.button("loading");

        if (languageradio.value.length > 0) {
            language = languageradio.value;
        }

        if (languageradio.value == 'all') {
            language = undefined;
        }

        Meteor.call(
            'generate.password',
            language,
            Number(passwordsize.value),
            function(error, cursor) {
                var password = "";

                if (error) {
                    console.log(error);
                } else {
                    cursor.forEach((word) => {
                        password += `${word.word} `;
                    });
                    if (password.length > 0) {
                        password = password.trim().charAt(0).toUpperCase() + password.slice(1).trim();
                        messageInput.val(`${password}.`);
                    }
                    button.button("reset");
                    button.attr("disabled", false);
                }
            });
    }
})