import { Meteor } from 'meteor/meteor';
import { Messages } from '../imports/api/messages.js';
import { Words } from '../imports/api/words.js';

var fs = Npm.require('fs');
var readline = Npm.require('readline');

// Stupid hack to make Node 4 act like Node 6.
if (fs.constants === undefined) {
  fs.constants = new Object();
  fs.constants.R_OK = fs.R_OK;
}

Meteor.startup(() => {
  // code to run on server at startup
  Meteor.setInterval(
    purge_messages_by_date,
    Meteor.settings.public.purging_by_date_ms
  );
  Meteor.setInterval(
    purge_messages_by_views,
    Meteor.settings.public.purging_by_views_ms
  );

  if (Meteor.settings.public.password_generator) {
    var wordlists = new Object(Meteor.settings.private.wordlists);
    var wordlist_file;
    for (key in wordlists) {
      // This path is apparently where meteor places all the files in ./public
      // from the repo root.
      wordlist_file = process.cwd() + '/../web.browser/app/' + wordlists[key];
      read_wordlist(wordlist_file, key);
    }
  }
});

function read_wordlist(filename, language) {
  // Prevent unnecessary counting of there are already words in collection.
  if (Words.find({language: language}).count() > 0) {
    return true;
  }

  console.log(`Reading wordlist for ${language}: ${filename}`)
  const rl = readline.createInterface({
    input: fs.createReadStream(filename)
  });

  rl.on('line', Meteor.bindEnvironment((line) => {
    Words.insert({word: line, language: language}, function(error, result) {
      if (error) {
        // Do not display "duplicate" errors.
        if (error.code != 11000) {
          console.log(error);
        }
      }
    });
    //var syncInsert = Meteor.wrapAsync(Words.insert);
  }));
}

function purge_messages_by_views() {
  var result = Messages.find(
    {},
    { views: { $gt: 0 }}
  );

  result.forEach((message) => {
    var viewsLeft = Math.ceil(
      message.maxViews - message.views
    );

    if (viewsLeft <= 0) {
      console.log(`Purging ID ${message._id} with ${viewsLeft} views left`);
      Messages.remove(message._id);
    }
  });
}

// Purge messages based on number of days.
function purge_messages_by_date() {
  var currentDate = new Date();

  var result = Messages.find(
    {},
    { fields: { createdAt: true, maxDays: true } }
  );

  result.forEach((message) => {
    var timeDelta = currentDate - message.createdAt;
    var deltaDays = (timeDelta/1000)/3600/24
    if (deltaDays > message.maxDays) {
      console.log(`Purging ID ${message._id} created ${message.createdAt} with max ${message.maxDays} days`)
      Messages.remove(message._id);
    }
  });
}